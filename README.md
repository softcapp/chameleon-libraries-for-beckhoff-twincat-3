# README #

Please follow the steps below to get you up and running with Chameleon.

### What to do? ###

* Download this repo as a zip file onto your computer and unzip the contents in any directory of your choice.
* Install the two libraries with 'Beckhoff Library Repository.".
* Add Chameleon library references to your project.
* Start coding.


### Download libraries ###
At the top of this screen, please click on the menu option and select 'Download repository'. Please see the following image for an illustration of what this looks like.

![Alt text](https://sn3301files.storage.live.com/y4mggp5l8MKbyHYSTaIwsm6I-eanZQEGN0XZDWodPLKmBNUUR86D8C1eZwZSAbjHZwylk0lPrzj4TKAYrD3tPKoA2NoS73kL4Hvs4b_B5MCcDCTa5xihAmtMn1LG_wS4QY1YmBxnegG6Q49ya7t4GSFxYVU0bsNAtjHTPE2bfx4a6c_iuvSmHIEUQQ8lFe0dYXf?width=482&height=345&cropmode=none)

Once you download the zip file, please unzip it anywhere you like. You may want to create a directory and unzip them there.

### Install ###

When you unzip, you will find the following files.

* Chameleon.compiled-library
* ChameleonPlcConfig.compiled-library

You have to install both of these files in Beckhoff's 'Library Repository'.

* Go to 'References' in your PLC project and right click on it.
* Select 'Library repository...' from the popup menu.
* Click on install.
* Find the downloaded library files and select 'Chameleon.compiled-library' and 'ChameleonPlcConfig.compiled-library'.

![Alt text](https://sn3301files.storage.live.com/y4mu9Bw6i5rTs8syPXgAAgk5Y9MxGN8rRvJ2k2uz53yGLfjpg2rwUuJ3EwKmUTzwWH-ldM3lBYP__ckli1iol2lbhen_ALmm6MqWNhkSxv50XzFgq6V1C8bUCMkBhQThaqkotrapu9UAGe_pIKmESXnREHFBA_hGOZ9J0qfUGg5wxLmkseJa7bG1oZkaBFGTDTV?width=920&height=545&cropmode=none)

### Add library references ###

* Go to 'References' in your PLC project and right click on it.
* This time select 'Add library...' from the popup menu.
* Go to the Miscellaneous section and select ChameleonPlcConfig.
* Click on ok.
* Repeat the same process for ChameleonPlcLib. Select 'Add library...' and add ChameleonPlcLib to the project. 

![Alt text](https://sn3301files.storage.live.com/y4muxPWMthOmTHT7h7nOt7BCinVwONRw70bhsTZzkqmg9reD3ida56bakPGWhdRElER3-pYdb7PSjirbBEEv9woy9yq1so5TaJ5DGUwjPEpA1knciHkbv1q40xaYxSn451wOyMkz-G87ywaKdw1LJmQdGXTTpkc_THAPXNxjxMTpnuY0YBJJ642s4vFUKjoz3gA?width=935&height=610&cropmode=none)


At this point, you are ready to use Chameleon.

### Start coding ###

#### Create your objects ####

Automation is all about controlling objects you have in your machine. Things like ‘heater’, ‘cooler’, ‘pump’, ‘valve’ are all objects you want to control. So, in Chameleon we have a function block that represents an object. Your first task is to make a list of your objects in your machine and create a, object that corresponds to it. Remember, each object will have the basic properties:
* Status (on or off)
* Set point
* Current value

Of course, these properties will not make sense for all objects but in general all objects will have some combination of these properties.  
   
So, here is how we create a new object:

* Create a new Function Block
* Extend from FB_CPObject
* Implement ITF_UnitState
* Add "Super^();" in code section
* Minimum override these functions ``` OnSetPointChange OnStatusChange OnUpdateCurrentStatus ```

![Alt text](https://sn3301files.storage.live.com/y4mTiz-3xmrfA0Qd2m2DDkCSHPc5mZGxBfPvAxUQUAexXs1wWDTLKSAWhuI5sQAx-eQkRJIBOopcrdtYGaUwwddCP8mk3OsuLs8lqB4YUgQLOEgQIvmAdN_iQ_AT7h1urlqQql0XmIbOlOZTugHC50fgk3P3kp9-79neWjfv5EKrDxGYkClhrnL7x4HWr6kO2mw?width=1039&height=568&cropmode=none)

#### Add your objects to the Chameleon library ####

Once you create your objects, you must instantiate them and add them to the Chameleon object list. At this point, some words about the Chameleon object list. Unfortunately, in PLC world, dynamic memory allocation is not possible. So, PLC code must have an object count during build time. For this reason, we build a configuration project where our users can configure a variable letting us know how many objects they have. So, please note that the object array length comes from this variable. Please see the config section below for more information.  

![Alt text](https://sn3301files.storage.live.com/y4mV7aFP8LEGttD9gjlP_SLcJ5aDB1yts7QOMW7_jBP7Apia3Bi4em1BkmqMhmHw-ezSi2Pl0uK9SIUg4jqEBLBL5lehC4Z3oOHg4ryKTgnuKUR2bRUSUCbjodwZySCzn7j7zCitCc4TOTYnLov89dUu-EazK8PTY7NWiGbrzr5rVOTARY8pr7casjmaeBV8ud-?width=1268&height=1400&cropmode=none)

#### Configuration ####

As we mentioned before, Chameleon project is all about objects. You need tell Chameleon how many objects your machine has so it can reserve the right amount of memory. In order to change the object count [please go to our repo](https://bitbucket.org/softcapp/chameleon-config-project-for-bechoff-twincat-3) and follow the instructions. It is a quite simple process. 


#### Sample project ####

We created a smaple project for our new user so they can start building Chameleon projects right away. [Please go to git repo](https://bitbucket.org/softcapp/chameleon-sample-project-for-bechoff-twincat-3/src/master/) and follow the instructions.